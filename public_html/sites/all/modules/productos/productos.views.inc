<?php

/**
 * @file
 * Defines portfolio views styles
 */

/**
 * Implements hook_views_plugins
 */
function productos_views_plugins() {
  return array(
      'style' => array(
          'productos' => array(
              'title' => t('Dexp Portfolio'),
              'help' => t('Display content in a portfolio grid.'),
              'handler' => 'productos_plugin_style_portfolio',
              'uses options' => TRUE,
              'uses row plugin' => TRUE,
              'uses row class' => TRUE,
              'type' => 'normal',
              'theme' => 'views_productos',
              'theme path' => drupal_get_path('module', 'productos') . '/theme',
              'theme file' => 'productos.theme.inc',
          ),
      ),
  );
}