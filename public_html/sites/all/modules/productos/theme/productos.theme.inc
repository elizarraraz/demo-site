<?php

/**
 * Preprocess function to build the dexp views portfolio
 * template_preprocess_dexp_portfolio
 */
function template_preprocess_views_productos(&$vars) {
  //template_preprocess_views_dexp_grid($vars);
  $view = $vars['view'];
  $options = $view->style_plugin->options;
  $vars['options'] = $options;
  $vars['view_id'] = drupal_html_id('page_dexp_portfolio');
  if ($options['productos_filter']) {
    $vocab = $options['productos_filter_vocabulary'];
    $categories = array();
    $taxonomies = taxonomy_get_tree($vocab);
    foreach ($taxonomies as $tax) {
      if ($tax->parents[0] == 0) {
        $key = _productos_safe_class($tax->name);
        $categories[$key] = $tax->name;
      }
    }
    $vars['categories'] = $categories;
    drupal_add_js('var ');
    drupal_add_js(drupal_get_path('module', 'productos') . '/js/jquery.shuffle.js');
    drupal_add_js(drupal_get_path('module', 'productos') . '/js/filter.js');
  }
  drupal_add_css(drupal_get_path('module', 'productos') . '/css/portfolio.css');
}