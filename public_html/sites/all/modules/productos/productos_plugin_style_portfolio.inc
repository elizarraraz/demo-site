<?php

/**
 * @file
 * Defines the style plugin for the Dexp Portfolio
 */
class productos_plugin_style_portfolio extends views_plugin_style {

  function option_definition() {
    $options = parent::option_definition();
    $options['productos_filter'] = array('default' => 0);
    $options['productos_filter_vocabulary'] = array('default' => 0);
    $options['productos_col_lg'] = array('default' => 3);
    $options['productos_col_md'] = array('default' => 3);
    $options['productos_col_sm'] = array('default' => 2);
    $options['productos_col_xs'] = array('default' => 1);
    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    
    $form['productos_col_lg'] = array(
        '#type' => 'select',
        '#title' => t('Large desktop columns'),
        '#options' => array(1=>1,2=>2,3=>3,4=>4,6=>6,12=>12),
        '#default_value' => $this->options['productos_col_lg']
    );
    $form['productos_col_md'] = array(
        '#type' => 'select',
        '#title' => t('Desktop columns'),
        '#options' => array(1=>1,2=>2,3=>3,4=>4,6=>6,12=>12),
        '#default_value' => $this->options['productos_col_md']
    );
    $form['productos_col_sm'] = array(
        '#type' => 'select',
        '#title' => t('Tablet columns'),
        '#options' => array(1=>1,2=>2,3=>3,4=>4,6=>6,12=>12),
        '#default_value' => $this->options['productos_col_sm']
    );
    $form['productos_col_xs'] = array(
        '#type' => 'select',
        '#title' => t('Phone columns'),
        '#options' => array(1=>1,2=>2,3=>3,4=>4,6=>6,12=>12),
        '#default_value' => $this->options['productos_col_xs']
    );
    $form['productos_filter'] = array(
        '#type' => 'checkbox',
        '#title' => t('Use Filter'),
        '#description' => t('Determines whether the filter is used on the portfolio.'),
        '#default_value' => $this->options['productos_filter']
    );

    $opts = array('- Select -');
    $tmp = array();
    foreach (taxonomy_vocabulary_get_names() as $vocab) {
      $tmp[$vocab->vid] = $vocab->name;
    }
    $opts += $tmp;
    $form['productos_filter_vocabulary'] = array(
        '#type' => 'select',
        '#title' => t('Filter Vocabulary'),
        '#options' => $opts,
        '#description' => t('Which taxonomy vocabulary do you want to use for the filter'),
        '#default_value' => $this->options['productos_filter_vocabulary'],
    );
  }

}