jQuery(document).ready(function($){
  $('.dexp-dropdown a.active').each(function(){
    $(this).parents('li.expanded').addClass('active');
  });
	$('.dexp-dropdown li.expanded').each(function(){
		var $this = $(this), $toggle = $('<span class="hidden-lg hidden-md menu-toggler fa fa-angle-right"></span>');
		$toggle.click(function(){
			$(this).toggleClass('fa-angle-right fa-angle-down');
			$this.find('>ul').toggleClass('menu-open');
		});
		$this.append($toggle);
	})
	$('.dexp-menu-toggler').click(function(){
		var $menu = $($(this).data('target'));
		if($menu != null){
			$menu.toggleClass('mobile-open');
		}
		return false;
	});
	$('.dexp-dropdown ul ul li').hover(function(){
		var $submenu = $(this).find('>ul');
		if($submenu.length == 0) return;
		if($submenu.offset().left + $submenu.width() > $(window).width()){
			$submenu.addClass('back');
		}
	}, function(){
		var $submenu = $(this).find('>ul');
		if($submenu.length == 0) return;
		$submenu.removeClass('back');
	})
})