<?php print $wrapper_prefix; ?>
  <?php if (!empty($title)) : ?>
    <h3><?php print $title; ?></h3>
  <?php endif; ?>
	<ul class="block-grid-lg-5 block-grid-md-5 block-grid-sm-1 block-grid-xs-1">
	    <?php foreach ($rows as $id => $row): ?>
	      <li class=" block-grid-item <?php print $classes_array[$id]; ?>"><?php print $row; ?></li>
	    <?php endforeach; ?>
	</ul>