(function ($, Drupal) {

  Drupal.behaviors.globo = {
    attach: function(context, settings) {
	    
	    
      // Get your Yeti started.
      $(document).anchor('hide');
      
     
      
      
      $(document).ready(function(){
	      
	    //$('#sucursales').anchor('');
	    
	   $("#block-webform-client-block-1242").affix({

        offset: { 

            }
    	});
    	
    	
	 
	 $("#zoom").mlens(
	{
		imgSrc: $("#zoom").attr("data-big"),	    // path of the hi-res version of the image
		//imgSrc2x: $("#zoom").attr("data-big2x"),  // path of the hi-res @2x version of the image
                                                                   //for retina displays (optional)
		lensShape: "circle",				// shape of the lens (circle/square)
		lensSize: 120,					// size of the lens (in px)
		borderSize: 4,					// size of the lens border (in px)
		borderColor: "#fff",				// color of the lens border (#hex)
		borderRadius: 0,				// border radius (optional, only if the shape is square)
		//imgOverlay: $("#zoom").attr("data-overlay"), // path of the overlay image (optional)
		overlayAdapt: false, // true if the overlay image has to adapt to the lens size (true/false)
		zoomLevel: 1.5                                    // zoom level multiplicator (number)
	});
	 
	 
	  });
	
	}
	
 };

})(jQuery, Drupal);