<?php

/**
 * @file
 * Display Suite Modal Boostrap configuration.
 */

function ds_modal_boostrap() {
  return array(
    'label' => t('Modal Boostrap'),
    'regions' => array(
	  'modal' => t('Modal'),  
      'visible' => t('Visible'),
      'visible_footer' => t('Visible Footer'),
    ),
    // Uncomment if you want to include a CSS file for this layout (modal_boostrap.css)
    // 'css' => TRUE,
    // Uncomment if you want to include a preview for this layout (modal_boostrap.png)
    // 'image' => TRUE,
  );
}
