<?php
/**
 * @file
 * Display Suite Producto Interior template.
 *
 * Available variables:
 *
 * Layout:
 * - $classes: String of classes that can be used to style this layout.
 * - $contextual_links: Renderable array of contextual links.
 * - $layout_wrapper: wrapper surrounding the layout.
 *
 * Regions:
 *
 * - $top: Rendered content for the "Top" region.
 * - $top_classes: String of classes that can be used to style the "Top" region.
 *
 * - $left: Rendered content for the "Left" region.
 * - $left_classes: String of classes that can be used to style the "Left" region.
 *
 * - $right: Rendered content for the "Right" region.
 * - $right_classes: String of classes that can be used to style the "Right" region.
 *
 * - $bottom: Rendered content for the "Bottom" region.
 * - $bottom_classes: String of classes that can be used to style the "Bottom" region.
 */
?>

<div class="container">
	
<<?php print $layout_wrapper; print $layout_attributes; ?> class="<?php print $classes;?> clearfix">



  <!-- Needed to activate contextual links -->
  <?php if (isset($title_suffix['contextual_links'])): ?>
    <?php print render($title_suffix['contextual_links']); ?>
  <?php endif; ?>

    <<?php print $top_wrapper; ?> class="col-sm-12 ds-top<?php print $top_classes; ?>">
      <?php print $top; ?>
    </<?php print $top_wrapper; ?>>

    <<?php print $left_wrapper; ?> class="ds-left col-sm-12 col-md-5 <?php print $left_classes; ?>">
      <?php print $left; ?>
    </<?php print $left_wrapper; ?>>

    <<?php print $right_wrapper; ?> class="ds-right col-sm-12 col-md-7 <?php print $right_classes; ?>">
      <div class="container-1"><?php print $right; ?></div>
      <?php if (!empty($right_bottom)): ?>
    <<?php print $right_bottom_wrapper; ?> class="ds-right-bottom  <?php print $right_bottom_classes; ?>">
       <div class="container-2"><?php print $right_bottom; ?></div>
    </<?php print $right_bottom_wrapper; ?>>
      <?php endif; ?>
    </<?php print $right_wrapper; ?>>
	
	
    <<?php print $bottom_wrapper; ?> class="col-sm-12 ds-bottom<?php print $bottom_classes; ?>">
      <?php print $bottom; ?>
    </<?php print $bottom_wrapper; ?>>
    
</<?php print $layout_wrapper ?>>
</div>

<!-- Needed to activate display suite support on forms -->
<?php if (!empty($drupal_render_children)): ?>
  <?php print $drupal_render_children ?>
<?php endif; ?>
