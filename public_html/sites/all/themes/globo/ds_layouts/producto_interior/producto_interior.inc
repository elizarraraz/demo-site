<?php

/**
 * @file
 * Display Suite Producto Interior configuration.
 */

function ds_producto_interior() {
  return array(
    'label' => t('Producto Interior'),
    'regions' => array(
      'top' => t('Top'),
      'left' => t('Left'),
      'right' => t('Right'),
      'right_bottom' => t('Right Bottom'),
      'bottom' => t('Bottom'),
    ),
    // Uncomment if you want to include a CSS file for this layout (producto_interior.css)
    // 'css' => TRUE,
    // Uncomment if you want to include a preview for this layout (producto_interior.png)
    // 'image' => TRUE,
  );
}
