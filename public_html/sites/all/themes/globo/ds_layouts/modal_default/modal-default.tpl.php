<?php
/**
 * @file
 * Display Suite Modal Default template.
 *
 * Available variables:
 *
 * Layout:
 * - $classes: String of classes that can be used to style this layout.
 * - $contextual_links: Renderable array of contextual links.
 * - $layout_wrapper: wrapper surrounding the layout.
 *
 * Regions:
 *
 * - $visible: Rendered content for the "Visible" region.
 * - $visible_classes: String of classes that can be used to style the "Visible" region.
 *
 * - $modal: Rendered content for the "Modal" region.
 * - $modal_classes: String of classes that can be used to style the "Modal" region.
 */
?>
<<?php print $layout_wrapper; print $layout_attributes; ?> class="<?php print $classes;?> clearfix">

  <!-- Needed to activate contextual links -->
  <?php if (isset($title_suffix['contextual_links'])): ?>
    <?php print render($title_suffix['contextual_links']); ?>
  <?php endif; ?>

    <<?php print $visible_wrapper; ?> class="ds-visible<?php print $visible_classes; ?>">
      <div data-toggle="modal" data-target="#myModalDefault">
	  <span><a href="#"><?php print $visible; ?></a></span>
	</div>
    </<?php print $visible_wrapper; ?>>

        <<?php print $modal_wrapper; ?> class="ds-modal<?php print $modal_classes; ?>">
      
      <!-- Modal -->
<div class="modal fade" id="myModalDefault" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="header-modal">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
       <div class="title">Catering</div>
      </div>
      <div class="modal-body">
        <?php print $modal; ?>
      </div>
    </div>
  </div>
</div>
    </<?php print $modal_wrapper; ?>>


</<?php print $layout_wrapper ?>>

<!-- Needed to activate display suite support on forms -->
<?php if (!empty($drupal_render_children)): ?>
  <?php print $drupal_render_children ?>
<?php endif; ?>
