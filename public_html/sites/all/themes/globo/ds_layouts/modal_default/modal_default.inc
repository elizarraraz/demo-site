<?php

/**
 * @file
 * Display Suite Modal Default configuration.
 */

function ds_modal_default() {
  return array(
    'label' => t('Modal Default'),
    'regions' => array(
      'visible' => t('Visible'),
      'modal' => t('Modal'),
    ),
    // Uncomment if you want to include a CSS file for this layout (modal_default.css)
    // 'css' => TRUE,
    // Uncomment if you want to include a preview for this layout (modal_default.png)
    // 'image' => TRUE,
  );
}
