<?php

/**
 * @file
 * Display Suite Promociones configuration.
 */

function ds_promociones() {
  return array(
    'label' => t('Promociones'),
    'regions' => array(
      'left' => t('Left'),
      'right' => t('Right')
    ),
    // Uncomment if you want to include a CSS file for this layout (producto_interior.css)
    // 'css' => TRUE,
    // Uncomment if you want to include a preview for this layout (producto_interior.png)
    // 'image' => TRUE,
  );
}
