<?php
/**
 * @file
 * html.vars.php
 *
 * @see html.tpl.php
 */

/**
 * Implements hook_preprocess_html().
 */
function bootstrap_preprocess_html(&$variables) {
  switch (theme_get_setting('bootstrap_navbar_position')) {
    case 'fixed-top':
      $variables['classes_array'][] = 'navbar-is-fixed-top';
      break;

    case 'fixed-bottom':
      $variables['classes_array'][] = 'navbar-is-fixed-bottom';
      break;

    case 'static-top':
      $variables['classes_array'][] = 'navbar-is-static-top';
      break;
  }
  
  if(arg(0)=='node' && is_numeric(arg(1))) {
        $node = node_load(arg(1)); 
        $results = taxonomy_node_get_terms($node);
        if(is_array($results)) {
            foreach ($results as $item) {
	            $name = preg_replace('[^a-z0-9]', '-', $item->name);
				$variables['classes_array'][] = "taxonomy-".strtolower(drupal_clean_css_identifier($name));
               
            }
       }
   }
   
}
