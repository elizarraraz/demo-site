<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * - $title : The title of this group of rows.  May be empty.
 * - $options['type'] will either be ul or ol.
 * @ingroup views_templates
 */
?>
<div class="text-center" data-equalizer>
<?php print $wrapper_prefix; ?>
  <?php if (!empty($title)) : ?>
    <h3><?php print $title; ?></h3>
  <?php endif; ?>
	<ul class="small-block-grid-1 medium-block-grid-4 large-block-grid-4 text-center">
	    <?php foreach ($rows as $id => $row): ?>
	      <li class=" marca <?php print $classes_array[$id]; ?>"><?php print $row; ?>test</li>
	    <?php endforeach; ?>
	</ul>
</div>